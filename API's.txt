+++++++++++++++++++++++++++++++++++++++++++++++++++++


List of API’s
POST API(lookup-data) body format:
{
    "listing":
        {
        "listing_name":"Test listing",
        "sale_rent": "sale",
        "prop_type": "Office",
        "location": "test location",
        "image_url":[],
        "asking_price":"something",
        "area": null,
        "bhk": null,
        "project": null,
        "update":"false",
        "locality": null,
        "floor_no": null,
        "total_floors": null,
        "furnishing": null,
        "additional_details": null,
        "visit_notice": null,
        "owner_name": null,
        "owner_number": null,
        "owner_address": null,
        },
    "prop_info":
        {
            "bssid":"test"
        },
    "mobile_info":
            {
                "brand":"test","device":"test","model":"test_data","imei":"test_imei","product":"test","mobile_id":"test","sdk":"test","release":"test"
            }
}

URL: 
POST: 52.66.125.36:8010/listings/lookup-data/ 
GET: 52.66.125.36:8010/listings/lookup-data/
On the launch of the app, make a POST request with:
"mobile_info":
			{
				"brand":"test","device":"test","model":"test_data","imei":"test_imei","product":"test","mobile_id":"test","sdk":"test","release":"test"
			}
on: 52.66.125.36:8010/listings/user-id/ 
It will return the response with an id, that will be userid
For a specific user:
52.66.125.36:8010/listings/lookup-data/?user=<user>


The image_url key in the POST request should send me the base64 string of an images, in an array. 
In the GET request I will be sending you the links of the image on the server, which will be comma separated string.

+++++++++++++++++++++++++++++++++++++++++++++++++++++

1.	Purchase Property

{
	"user": 32,
	"phone":"+918130330802",
	"listing_id": 87
}
URL: 52.66.125.36:8010/listings/purchase/ 

+++++++++++
