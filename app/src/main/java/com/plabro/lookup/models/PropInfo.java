package com.plabro.lookup.models;

/**
 * Propinfo
 * Created by sid on 3/2/18.
 */

public class PropInfo {
    private String bssid;

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }
}
