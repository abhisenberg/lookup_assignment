package com.plabro.lookup.models;

import java.util.List;

/**
 * Created by sid on 3/4/18.
 */

public class RecordData {

    private List<Listing> results;

    private String previous;

    private String count;

    private String next;

    private MobileInfo mobile_info;

    private PropInfo prop_info;

    public MobileInfo getMobile_info() {
        return mobile_info;
    }

    public void setMobile_info(MobileInfo mobile_info) {
        this.mobile_info = mobile_info;
    }

    public PropInfo getProp_info() {
        return prop_info;
    }

    public void setProp_info(PropInfo prop_info) {
        this.prop_info = prop_info;
    }

    public List<Listing> getResults() {
        return results;
    }

    public void setResults(List<Listing> results) {
        this.results = results;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

}
