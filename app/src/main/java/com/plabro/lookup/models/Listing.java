package com.plabro.lookup.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Results
 * Created by sid on 3/2/18.
 */

public class Listing implements Parcelable {

    public static final Creator<Listing> CREATOR = new Creator<Listing>() {
        @Override
        public Listing createFromParcel(Parcel in) {
            return new Listing(in);
        }

        @Override
        public Listing[] newArray(int size) {
            return new Listing[size];
        }
    };
    private int id;
    private String listing_name;
    private String sale_rent;
    private String prop_type;
    private String location;
    private String image_url;
    private String asking_price;
    private String area;
    private String bhk;
    private String locality;
    private String floor_no;
    private String total_floors;
    private String furnishing;
    private String additional_details;
    private String visit_notice;
    private String owner_name;
    private String owner_number;
    private String owner_address;
    private String insertion_time;
    private String bssid;

    public Listing() {
    }

    protected Listing(Parcel in) {
        id = in.readInt();
        listing_name = in.readString();
        sale_rent = in.readString();
        prop_type = in.readString();
        location = in.readString();
        image_url = in.readString();
        asking_price = in.readString();
        area = in.readString();
        bhk = in.readString();
        locality = in.readString();
        floor_no = in.readString();
        total_floors = in.readString();
        furnishing = in.readString();
        additional_details = in.readString();
        visit_notice = in.readString();
        owner_name = in.readString();
        owner_number = in.readString();
        owner_address = in.readString();
        insertion_time = in.readString();
        bssid = in.readString();
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getListing_name() {
        return listing_name;
    }

    public void setListing_name(String listing_name) {
        this.listing_name = listing_name;
    }

    public String getSaleRent() {
        return sale_rent;
    }

    public void setSaleRent(String saleRent) {
        this.sale_rent = saleRent;
    }

    public String getProp_type() {
        return prop_type;
    }

    public void setProp_type(String prop_type) {
        this.prop_type = prop_type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getAskingPrice() {
        return asking_price;
    }

    public void setAskingPrice(String askingPrice) {
        this.asking_price = askingPrice;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBhk() {
        return bhk;
    }

    public void setBhk(String bhk) {
        this.bhk = bhk;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getFloorNo() {
        return floor_no;
    }

    public void setFloorNo(String floorNo) {
        this.floor_no = floorNo;
    }

    public String getTotalFloors() {
        return total_floors;
    }

    public void setTotalFloors(String totalFloors) {
        this.total_floors = totalFloors;
    }

    public String getFurnishing() {
        return furnishing;
    }

    public void setFurnishing(String furnishing) {
        this.furnishing = furnishing;
    }

    public String getAdditionalDetails() {
        return additional_details;
    }

    public void setAdditionalDetails(String additionalDetails) {
        this.additional_details = additionalDetails;
    }

    public String getVisitNotice() {
        return visit_notice;
    }

    public void setVisitNotice(String visitNotice) {
        this.visit_notice = visitNotice;
    }

    public String getOwnerName() {
        return owner_name;
    }

    public void setOwnerName(String ownerName) {
        this.owner_name = ownerName;
    }

    public String getOwnerNumber() {
        return owner_number;
    }

    public void setOwnerNumber(String ownerNumber) {
        this.owner_number = ownerNumber;
    }

    public String getOwnerAddress() {
        return owner_address;
    }

    public void setOwnerAddress(String ownerAddress) {
        this.owner_address = ownerAddress;
    }

    public String getInsertion_time() {
        return insertion_time;
    }

    public void setInsertion_time(String insertion_time) {
        this.insertion_time = insertion_time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(listing_name);
        dest.writeString(sale_rent);
        dest.writeString(prop_type);
        dest.writeString(location);
        dest.writeString(image_url);
        dest.writeString(asking_price);
        dest.writeString(area);
        dest.writeString(bhk);
        dest.writeString(locality);
        dest.writeString(floor_no);
        dest.writeString(total_floors);
        dest.writeString(furnishing);
        dest.writeString(additional_details);
        dest.writeString(visit_notice);
        dest.writeString(owner_name);
        dest.writeString(owner_number);
        dest.writeString(owner_address);
        dest.writeString(insertion_time);
        dest.writeString(bssid);
    }
}
