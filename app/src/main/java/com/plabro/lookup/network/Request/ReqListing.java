package com.plabro.lookup.network.Request;

/**
 * Created by sid on 3/6/18.
 */

public class ReqListing {

    private String listing_name;
    private String sale_rent;
    private String prop_type;
    private String location;
    private String[] image_url;
    private String asking_price;
    private String area;
    private String bhk;
    private String locality;
    private String floor_no;
    private String total_floors;
    private String furnishing;
    private String additional_details;
    private String visit_notice;
    private String owner_name;
    private String owner_number;
    private String owner_address;
    private String insertion_time;
    private String bssid;

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public String getListing_name() {
        return listing_name;
    }

    public void setListing_name(String listing_name) {
        this.listing_name = listing_name;
    }

    public String getSaleRent() {
        return sale_rent;
    }

    public void setSaleRent(String saleRent) {
        this.sale_rent = saleRent;
    }

    public String getProp_type() {
        return prop_type;
    }

    public void setProp_type(String prop_type) {
        this.prop_type = prop_type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String[] getImage_url() {
        return image_url;
    }

    public void setImage_url(String[] image_url) {
        this.image_url = image_url;
    }

    public String getAskingPrice() {
        return asking_price;
    }

    public void setAskingPrice(String askingPrice) {
        this.asking_price = askingPrice;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBhk() {
        return bhk;
    }

    public void setBhk(String bhk) {
        this.bhk = bhk;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getFloorNo() {
        return floor_no;
    }

    public void setFloorNo(String floorNo) {
        this.floor_no = floorNo;
    }

    public String getTotalFloors() {
        return total_floors;
    }

    public void setTotalFloors(String totalFloors) {
        this.total_floors = totalFloors;
    }

    public String getFurnishing() {
        return furnishing;
    }

    public void setFurnishing(String furnishing) {
        this.furnishing = furnishing;
    }

    public String getAdditionalDetails() {
        return additional_details;
    }

    public void setAdditionalDetails(String additionalDetails) {
        this.additional_details = additionalDetails;
    }

    public String getVisitNotice() {
        return visit_notice;
    }

    public void setVisitNotice(String visitNotice) {
        this.visit_notice = visitNotice;
    }

    public String getOwnerName() {
        return owner_name;
    }

    public void setOwnerName(String ownerName) {
        this.owner_name = ownerName;
    }

    public String getOwnerNumber() {
        return owner_number;
    }

    public void setOwnerNumber(String ownerNumber) {
        this.owner_number = ownerNumber;
    }

    public String getOwnerAddress() {
        return owner_address;
    }

    public void setOwnerAddress(String ownerAddress) {
        this.owner_address = ownerAddress;
    }

    public String getInsertion_time() {
        return insertion_time;
    }

    public void setInsertion_time(String insertion_time) {
        this.insertion_time = insertion_time;
    }
}
