package com.plabro.lookup.network.Request;

import com.plabro.lookup.models.MobileInfo;
import com.plabro.lookup.models.PropInfo;

/**
 * Created by sid on 3/4/18.
 */

public class ReqRecordData {

    private ReqListing listing;

    private String previous;

    private String count;

    private String next;

    private MobileInfo mobile_info;

    private PropInfo prop_info;

    public MobileInfo getMobile_info() {
        return mobile_info;
    }

    public void setMobile_info(MobileInfo mobile_info) {
        this.mobile_info = mobile_info;
    }

    public PropInfo getProp_info() {
        return prop_info;
    }

    public void setProp_info(PropInfo prop_info) {
        this.prop_info = prop_info;
    }

    public ReqListing getListing() {
        return listing;
    }

    public void setListing(ReqListing listing) {
        this.listing = listing;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

}
