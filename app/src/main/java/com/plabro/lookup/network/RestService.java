package com.plabro.lookup.network;

import com.plabro.lookup.models.Listing;
import com.plabro.lookup.models.RecordData;
import com.plabro.lookup.network.Request.ReqMobileInfo;
import com.plabro.lookup.network.Request.ReqRecordData;
import com.plabro.lookup.network.Response.ResUserId;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by sid on 3/4/18.
 */

public interface RestService {

    @POST("/listings/lookup-data/")
    Call<Listing> postListing(@Body ReqRecordData results);

    @POST("/listings/user-id/")
    Call<ResUserId> getUserId(@Body ReqMobileInfo reqMobileInfo);

    @GET("/listings/lookup-data/")
    Call<RecordData> getListings();

    @GET("/listings/lookup-data/")
    Call<RecordData> getListings(@Query("user") int id);

    @GET("/listings/lookup-data/")
    Call<RecordData> getListings2(@Query("user") int id, @Query("page") int page);


}
