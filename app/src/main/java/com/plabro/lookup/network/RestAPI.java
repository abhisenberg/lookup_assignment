package com.plabro.lookup.network;

import com.plabro.lookup.Util.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by sid on 3/4/18.
 */

public class RestAPI {
    public static Retrofit adapter = new Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    public static RestService appService;

    public static RestService getAppService() {
        appService = adapter.create(RestService.class);
        return appService;
    }
}
