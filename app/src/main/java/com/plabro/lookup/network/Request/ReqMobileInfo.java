package com.plabro.lookup.network.Request;

import com.plabro.lookup.models.MobileInfo;

/**
 * Created by sid on 3/8/18.
 */

public class ReqMobileInfo {
    MobileInfo mobile_info;

    public MobileInfo getMobile_info() {
        return mobile_info;
    }

    public void setMobile_info(MobileInfo mobile_info) {
        this.mobile_info = mobile_info;
    }
}
