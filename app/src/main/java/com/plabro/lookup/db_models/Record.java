package com.plabro.lookup.db_models;

import com.orm.SugarRecord;

/**
 * Created by sid on 3/2/18.
 */

public class Record extends SugarRecord {

    public int recordId;
    public String listing;
    public String propInfo;
    public String mobileInfo;

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public String getListing() {
        return listing;
    }

    public void setListing(String listing) {
        this.listing = listing;
    }

    public String getPropInfo() {
        return propInfo;
    }

    public void setPropInfo(String propInfo) {
        this.propInfo = propInfo;
    }

    public String getMobileInfo() {
        return mobileInfo;
    }

    public void setMobileInfo(String mobileInfo) {
        this.mobileInfo = mobileInfo;
    }
}
