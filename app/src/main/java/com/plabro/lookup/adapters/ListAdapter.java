package com.plabro.lookup.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.plabro.lookup.R;
import com.plabro.lookup.Util.Constants;
import com.plabro.lookup.activity.DetailsActivity;
import com.plabro.lookup.models.Listing;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sid on 3/2/18.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    List<Listing> list;
    Context context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Listing listing = list.get(position);
        holder.title.setText(listing.getListing_name());
        holder.whose.setText(listing.getOwnerName());

        try {
            if (listing.getImage_url().contains(",")) {
                String[] sp = listing.getImage_url().split(",");
                Picasso.with(context).load(sp[0]).resize(600, 0).into(holder.image);
            } else {
                Picasso.with(context).load(listing.getImage_url()).resize(600, 0).into(holder.image);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public void swapData(List<Listing> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.title)
        TextView title;

        @BindView(R.id.shareImage)
        ImageView shareImage;

        @BindView(R.id.shareText)
        TextView shareText;

        @BindView(R.id.whose)
        TextView whose;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Listing listing = list.get(getAdapterPosition());
                    Intent intent = new Intent(context, DetailsActivity.class);
                    intent.putExtra(Constants.DETAIL_RECORD_DATA, listing);
                    context.startActivity(intent);
                }
            });
        }
    }
}
