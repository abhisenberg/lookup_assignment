package com.plabro.lookup.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.plabro.lookup.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter for imageRecycler in @DetailsActivity
 * Created by sid on 3/1/18.
 */

public class EditListImageAdapter extends RecyclerView.Adapter<EditListImageAdapter.ViewHolder> {

    int position = 0;
    private List<String> list;
    private Context context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_edit_list_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String path = list.get(position);
        if (!path.equals("")) {
            if (path.contains("emulated")) {
                File image = new File(path);
                Picasso.with(context).load(image).resize(600, 0).into(holder.image);
            } else {
                Picasso.with(context).load(path).resize(600, 0).into(holder.image);
            }
        }
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public void swapData(List<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
