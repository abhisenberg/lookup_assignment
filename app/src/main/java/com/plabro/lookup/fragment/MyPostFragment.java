package com.plabro.lookup.fragment;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.plabro.lookup.R;
import com.plabro.lookup.activity.MainActivity;
import com.plabro.lookup.activity.ShowListsActivity;
import com.plabro.lookup.adapters.ListAdapter;
import com.plabro.lookup.models.Listing;
import com.plabro.lookup.models.MobileInfo;
import com.plabro.lookup.models.RecordData;
import com.plabro.lookup.network.Request.ReqMobileInfo;
import com.plabro.lookup.network.Response.ResUserId;
import com.plabro.lookup.network.RestAPI;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */

public class MyPostFragment extends Fragment {

    private static final String TAG = "MyPostFragment";
    static final int REQUEST_PHONE_STATE_PERMISSION = 1;
    public int page = 1;
    public int id;
    @BindView(R.id.recyclerView_mypost)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    View view;
    String imei;
    MobileInfo mobileInfo;
    ListAdapter listAdapter;
    List<Listing> list = new ArrayList<>();

    public MyPostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_my_post, container, false);

        ButterKnife.bind(this, view);

        listAdapter = new ListAdapter();
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listAdapter);

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PHONE_STATE_PERMISSION);
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                imei = telephonyManager.getDeviceId();
                getUserId();
            }
        }

        return view;
    }

    public void getData(int id) {
        progressBar.setVisibility(View.VISIBLE);
        RestAPI.getAppService().getListings(id).enqueue(new Callback<RecordData>() {
            @Override
            public void onResponse(Call<RecordData> call, Response<RecordData> response) {
                try {
                    list.clear();
                    List<Listing> results = response.body().getResults();
                    Log.d(TAG, "Number of listings of this user: "+results.size());
                    for (int i = 0; i < results.size(); i++) {
                        list.add(results.get(i));
                    }
                    listAdapter.swapData(list);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<RecordData> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    public MobileInfo setMobileInfo1() {
        mobileInfo = new MobileInfo();
        mobileInfo.setBrand(Build.BRAND);
        mobileInfo.setDevice(Build.DEVICE);
        mobileInfo.setMobile_id(Build.ID);
        mobileInfo.setImei(imei);
        mobileInfo.setModel(Build.MODEL);
        mobileInfo.setSdk(Build.VERSION.SDK);
        return mobileInfo;
    }

    public void getUserId() {
        progressBar.setVisibility(View.VISIBLE);
        ReqMobileInfo reqMobileInfo = new ReqMobileInfo();
        reqMobileInfo.setMobile_info(setMobileInfo1());
        String s = new Gson().toJson(reqMobileInfo, ReqMobileInfo.class);
        RestAPI.getAppService().getUserId(reqMobileInfo).enqueue(new Callback<ResUserId>() {
            @Override
            public void onResponse(Call<ResUserId> call, Response<ResUserId> response) {
                try {
                    id = response.body().getId();
                    if (id != 0) {
                        Log.d(TAG, "onResponse: "+id);
                        getData(id);
                    } else {
                        Toast.makeText(getActivity(), "No listings found", Toast.LENGTH_SHORT).show();
                    }
                    progressBar.setVisibility(View.INVISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResUserId> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(), "Connection failure", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
