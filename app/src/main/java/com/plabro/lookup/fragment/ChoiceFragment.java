package com.plabro.lookup.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.plabro.lookup.R;
import com.plabro.lookup.Util.Constants;
import com.plabro.lookup.activity.DetailsActivity;
import com.plabro.lookup.activity.ShowListsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChoiceFragment extends Fragment {

    @BindView(R.id.captureFab)
    FloatingActionButton captureFab;
    @BindView(R.id.listingsFab)
    FloatingActionButton listingsFab;
    @BindView(R.id.listingsText)
    TextView listingsText;
    @BindView(R.id.captureText)
    TextView captureText;

    public ChoiceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choice, container, false);
        ButterKnife.bind(this, view);
        init(view);
        return view;
    }

    private void init(View view) {

        //To exit app on back press of fragment
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    getActivity().finish();
                    return true;
                }
                return false;
            }
        });


    }

    @OnClick({R.id.captureText, R.id.captureFab})
    public void openDetailActivity() {
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(Constants.OPEN_CAMERA, true);
        startActivity(intent);
    }

    @OnClick({R.id.listingsText, R.id.listingsFab})
    public void openListingActivity() {
        Intent intent = new Intent(getActivity(), ShowListsActivity.class);
        startActivity(intent);
    }


}
