package com.plabro.lookup.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.plabro.lookup.R;
import com.plabro.lookup.activity.ShowListsActivity;
import com.plabro.lookup.adapters.ListAdapter;
import com.plabro.lookup.models.Listing;
import com.plabro.lookup.models.RecordData;
import com.plabro.lookup.network.RestAPI;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllPostsFragment extends Fragment {

    private static final String TAG = "AllPostFragment";
    @BindView(R.id.recyclerView_allposts)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    View view;
    ListAdapter listAdapter;
    List<Listing> list = new ArrayList<>();

    public AllPostsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_all_posts, container, false);

        ButterKnife.bind(this, view);

        listAdapter = new ListAdapter();
        final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(listAdapter);

        getData();

        return view;
    }

    public void getData() {
        progressBar.setVisibility(View.VISIBLE);
        RestAPI.getAppService().getListings().enqueue(new Callback<RecordData>() {
            @Override
            public void onResponse(Call<RecordData> call, Response<RecordData> response) {
                try {
                    list.clear();
                    List<Listing> results = response.body().getResults();
                    Log.d(TAG, "Number of listings total "+results.size());
                    for (int i = 0; i < results.size(); i++) {
                        list.add(results.get(i));
                    }
                    listAdapter.swapData(list);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<RecordData> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

}
