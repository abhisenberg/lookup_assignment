package com.plabro.lookup.Util;

import java.util.Calendar;
import java.util.Locale;

/**
 * Static methods here
 * Created by sid on 3/2/18.
 */

public class Utility {

    public static String returnDateText(String timestamp) {
        long timeLong = Long.parseLong(timestamp);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeLong);
        String dayOfMonth = addOrdinalIndicatorToDate(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        String time = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + " " + calendar.getDisplayName(Calendar.AM_PM, Calendar.LONG, Locale.getDefault());
        return dayOfMonth + " " + month + " " + year + " " + time;
    }

    private static String addOrdinalIndicatorToDate(String day) {
        int n = Integer.parseInt(day);
        if (n >= 1 && n <= 31) {
            if (n >= 11 && n <= 13) {
                return day + "th";
            }
            switch (n % 10) {
                case 1:
                    return day + "st";
                case 2:
                    return day + "nd";
                case 3:
                    return day + "rd";
                default:
                    return day + "th";
            }
        } else {
            return day;
        }
    }
}
