package com.plabro.lookup.Util;

/**
 * Contains all the app constants
 * Created by sid on 3/1/18.
 */

public class Constants {

    public static final String BASE_URL = "http://52.66.125.36:8010";
    public static final String OPEN_CAMERA = "OPEN_CAMERA";
    public static final String DETAIL_RECORD_ID = "DETAIL_RECORD_ID";
    public static final String DETAIL_RECORD_DATA = "DETAIL_RECORD_DATA";
    public static final String CURRENT_USER_ID = "CURRENT_USER_ID";

}
