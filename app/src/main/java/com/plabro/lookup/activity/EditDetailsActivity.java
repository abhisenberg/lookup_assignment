package com.plabro.lookup.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.plabro.lookup.R;
import com.plabro.lookup.Util.Constants;
import com.plabro.lookup.db_models.Record;
import com.plabro.lookup.models.Listing;
import com.plabro.lookup.models.MobileInfo;
import com.plabro.lookup.models.PropInfo;
import com.plabro.lookup.network.Request.ReqListing;
import com.plabro.lookup.network.Request.ReqRecordData;
import com.plabro.lookup.network.RestAPI;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditDetailsActivity extends AppCompatActivity {

    private static final String TAG = EditDetailsActivity.class.getSimpleName();

    static final int REQUEST_PHONE_STATE_PERMISSION = 1;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.save)
    TextView save;
    @BindView(R.id.cancel)
    CardView cancel;
    @BindView(R.id.saveCard)
    CardView saveCard;
    @BindView(R.id.locationCard)
    CardView locationCard;
    @BindView(R.id.title)
    EditText title;
    @BindView(R.id.propertyForText)
    TextView propertyForText;
    @BindView(R.id.propertyTypeText)
    TextView propertyTypeText;
    @BindView(R.id.askingPriceText)
    TextView askingPriceText;
    @BindView(R.id.areaText)
    TextView areaText;
    @BindView(R.id.areaUnitText)
    TextView areaUnitText;
    @BindView(R.id.askingPriceUnitText)
    TextView askingPriceUnitText;
    @BindView(R.id.roomsText)
    TextView roomsText;
    @BindView(R.id.localText)
    EditText localText;
    @BindView(R.id.floorText)
    EditText floorText;
    @BindView(R.id.floorCountText)
    EditText floorCountText;
    @BindView(R.id.furnishingText)
    TextView furnishingText;
    @BindView(R.id.addInfoText)
    EditText addInfoText;
    @BindView(R.id.noticeText)
    TextView noticeText;
    @BindView(R.id.noticeUnitText)
    TextView noticeUnitText;
    @BindView(R.id.ownerNameText)
    EditText ownerNameText;
    @BindView(R.id.phoneText)
    EditText phoneText;
    @BindView(R.id.addressText)
    EditText addressText;
    @BindView(R.id.locationText)
    TextView locationText;
    @BindView(R.id.forwardIcon)
    ImageView forwardIcon;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    int recordId = 0;
    Record listingRecord;
    MobileInfo mobileInfo;
    PropInfo propInfo;
    ReqListing reqListing;
    Listing listing = new Listing();
    int PLACE_PICKER_REQUEST = 1;
    String imei = "";

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_details);
        ButterKnife.bind(this);


        try {
            Log.d(TAG, "Opened by: "+ getIntent().getComponent().getClassName());
            listing = new Gson().fromJson(getIntent().getExtras().getString(Constants.DETAIL_RECORD_DATA), Listing.class);
            listingRecord = new Record();
            loadData();
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            finish();
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PHONE_STATE_PERMISSION);
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                imei = telephonyManager.getDeviceId();
            }
        }

    }

    public void convertImages() {

    }

    public void loadData() {
        title.setText(listing.getListing_name());
        propertyForText.setText(listing.getSaleRent());
        propertyTypeText.setText(listing.getProp_type());
        localText.setText(listing.getLocation());
        roomsText.setText(listing.getBhk());
        localText.setText(listing.getLocality());
        floorText.setText(listing.getFloorNo());
        floorCountText.setText(listing.getTotalFloors());
        furnishingText.setText(listing.getFurnishing());
        addInfoText.setText(listing.getAdditionalDetails());
        ownerNameText.setText(listing.getOwnerName());
        phoneText.setText(listing.getOwnerNumber());
        addressText.setText(listing.getOwnerAddress());
        locationText.setText(listing.getLocation());

        convertImages();

        String ap = listing.getAskingPrice();
        ap = ap.replaceAll(" ", "");
        if (ap.contains("Rs") || ap.contains("Others")) {
            String apNumber = ap.replaceAll("[^\\d]", "");
            String apChar = ap.replaceAll("[\\d]", "");
            askingPriceUnitText.setText(apChar);
            askingPriceText.setText(apNumber);
        } else {
            askingPriceText.setText(listing.getAskingPrice());
        }

        String a = listing.getArea();
        a = a.replaceAll(" ", "");

        if (a.contains("Sq") || a.contains("Acre") || a.contains("Others")) {
            String aNumber = a.replaceAll("[^\\d]", "");
            String aChar = a.replaceAll("[\\d]", "");
            areaUnitText.setText(aChar);
            areaText.setText(aNumber);
        } else {
            areaText.setText(listing.getArea());
        }

        String n = listing.getVisitNotice();
        n = n.replaceAll(" ", "");

        if (n.contains("Hour") || n.contains("Day") || n.contains("Week") || n.contains("Month")) {
            String nNumber = n.replaceAll("[^\\d]", "");
            String nChar = n.replaceAll("[\\d]", "");
            noticeUnitText.setText(nChar);
            noticeText.setText(nNumber);
        } else {
            noticeText.setText(listing.getVisitNotice());
        }

    }

    public void setListing() {
        listing.setListing_name(title.getText().toString());
        listing.setSaleRent(propertyForText.getText().toString());
        listing.setProp_type(propertyTypeText.getText().toString());
        listing.setLocation(locationText.getText().toString());
        listing.setAskingPrice(askingPriceText.getText().toString() + " " + askingPriceUnitText.getText().toString());
        listing.setArea(areaText.getText().toString() + " " + areaUnitText.getText().toString());
        listing.setBhk(roomsText.getText().toString());
        listing.setLocality(localText.getText().toString());
        listing.setFloorNo(floorText.getText().toString());
        listing.setTotalFloors(floorCountText.getText().toString());
        listing.setFurnishing(furnishingText.getText().toString());
        listing.setAdditionalDetails(addInfoText.getText().toString());
        listing.setVisitNotice(noticeText.getText().toString() + " " + noticeUnitText.getText().toString());
        listing.setOwnerName(ownerNameText.getText().toString());
        listing.setOwnerNumber(phoneText.getText().toString());
        listing.setOwnerAddress(addressText.getText().toString());
//        try {
//            listingRecord.setListing(new Gson().toJson(listing, Listing.class));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        if(listing == null) Log.d(TAG, "setListing: LISTING IS NULL.");
        else Log.d(TAG, "setListing: LISTING IS NOT NULL");

        String edittedJSON = new Gson().toJson(listing, Listing.class);
        Log.d(TAG, "JSON string is: "+edittedJSON);

        listingRecord.setListing(edittedJSON);
    }

    public MobileInfo setMobileInfo1() {
        mobileInfo = new MobileInfo();
        mobileInfo.setBrand(Build.BRAND);
        mobileInfo.setDevice(Build.DEVICE);
        mobileInfo.setMobile_id(Build.ID);
        mobileInfo.setImei(imei);
        mobileInfo.setModel(Build.MODEL);
        mobileInfo.setSdk(Build.VERSION.SDK);
        return mobileInfo;
    }

    public PropInfo setProp() {
        propInfo = new PropInfo();
        propInfo.setBssid("");
        return propInfo;
    }

    public ReqListing setReqList() {
        reqListing = new ReqListing();
        reqListing.setAdditionalDetails(listing.getAdditionalDetails());
        reqListing.setArea(listing.getArea());
        reqListing.setAskingPrice(listing.getAskingPrice());
        reqListing.setBhk(listing.getBhk());
        reqListing.setBssid(listing.getBssid());
        reqListing.setFloorNo(listing.getFloorNo());
        reqListing.setFurnishing(listing.getFurnishing());
        reqListing.setInsertion_time(listing.getInsertion_time());
        reqListing.setListing_name(listing.getListing_name());
        reqListing.setLocation(listing.getLocation());
        reqListing.setLocality(listing.getLocality());
        reqListing.setOwnerAddress(listing.getOwnerAddress());
        reqListing.setOwnerNumber(listing.getOwnerNumber());
        reqListing.setOwnerName(listing.getOwnerName());
        reqListing.setProp_type(listing.getProp_type());
        reqListing.setSaleRent(listing.getSaleRent());
        reqListing.setVisitNotice(listing.getVisitNotice());

        ImageGetter imageGetter = new ImageGetter();
        if (listing.getImage_url().contains(",")) {
            String[] arrayS = listing.getImage_url().split(",");
            for (int i = 0; i < arrayS.length; i++) {
                Bitmap bm = null;
                if(arrayS[i].contains("/storage/")){
                    bm = BitmapFactory.decodeFile(arrayS[i]);
                } else {
                    try {
                        bm = BitmapFactory.decodeStream(imageGetter.execute(arrayS[i]).get());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 30, baos); //bm is the bitmap object
                byte[] b = baos.toByteArray();
                arrayS[i] = Base64.encodeToString(b, Base64.DEFAULT);
            }
            reqListing.setImage_url(arrayS);
        } else {
            Bitmap bm = null;
            if(listing.getImage_url().contains("/storage/")){
                bm = BitmapFactory.decodeFile(listing.getImage_url());
            } else {
                try {
                    bm = BitmapFactory.decodeStream(imageGetter.execute(listing.getImage_url()).get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
            Log.d(TAG, "Bitmap image url = "+listing.getImage_url());
            if(bm == null) Log.d(TAG, "BITMAP IS NULL ");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 30, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            String[] a = new String[1];
            a[0] = Base64.encodeToString(b, Base64.DEFAULT);
            reqListing.setImage_url(a);
        }
        return reqListing;
    }

    private static class ImageGetter extends AsyncTask<String, Void, InputStream> {

        @Override
        protected InputStream doInBackground(String... stringURL) {
            try {
                URL url = new URL(stringURL[0]);
                return url.openConnection().getInputStream();
            } catch (MalformedURLException e) {
                Log.d(TAG, "getImageStream: IMAGE URL NOT VALID");
                e.printStackTrace();
            } catch (IOException e) {
                Log.d(TAG, "getImageStream: CANNOT OPEN URL CONNCETION");
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * Click listeners
     */

    @OnClick({R.id.save, R.id.saveCard})
    public void saveEntry() {
        progressBar.setVisibility(View.VISIBLE);
        setListing();
//        try {
//            listingRecord.save();
//        } catch (Exception e) {
//            Log.d(TAG, "saveEntry: FAILED TO SAVE RECORD");
//            e.printStackTrace();
//        }
        Log.d(TAG, "saveEntry: Trying to save the entry.");
        listingRecord.save();
        Log.d(TAG, "saveEntry: Entry saved.");

        ReqRecordData recordData = new ReqRecordData();
        recordData.setListing(setReqList());
        recordData.setMobile_info(setMobileInfo1());
        recordData.setProp_info(setProp());

        String s = new Gson().toJson(recordData, ReqRecordData.class);

        Log.d(TAG, "saveEntry: Making REST call to upload the entry.");
        RestAPI.getAppService().postListing(recordData).enqueue(new Callback<Listing>() {
            @Override
            public void onResponse(Call<Listing> call, Response<Listing> response) {
                Toast.makeText(EditDetailsActivity.this, "Successfully editted.", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<Listing> call, Throwable t) {
                Toast.makeText(EditDetailsActivity.this, "Failed to save listing", Toast.LENGTH_LONG).show();
            }
        });
    }

    @OnClick({R.id.back, R.id.cancel})
    public void closeActivity() {
        finish();
    }

    @OnClick(R.id.propertyForText)
    public void propertyForTextClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Property For");
        final String[] items = getResources().getStringArray(R.array.propertyFor);
        builder.setSingleChoiceItems(items, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        propertyForText.setText(items[which]);
                    }
                });
        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        propertyForText.setText(items[0]);
    }

    @OnClick(R.id.areaUnitText)
    public void areaUnitTextClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Unit");
        final String[] items = getResources().getStringArray(R.array.areaUnit);
        builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                areaUnitText.setText(items[which]);
            }
        });
        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        areaUnitText.setText(items[0]);
    }

    @OnClick(R.id.askingPriceUnitText)
    public void askingPriceUnitTextClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Currency");
        final String[] items = getResources().getStringArray(R.array.askingUnit);
        builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                askingPriceUnitText.setText(items[which]);
            }
        });
        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        askingPriceUnitText.setText(items[0]);
    }

    @OnClick(R.id.propertyTypeText)
    public void propertyTypeTextClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Property Type");
        final String[] items = getResources().getStringArray(R.array.propertyType);
        builder.setSingleChoiceItems(items, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        propertyTypeText.setText(items[which]);
                    }
                });
        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        propertyTypeText.setText(items[0]);
    }

    @OnClick(R.id.roomsText)
    public void roomsTextClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Rooms Type");
        final String[] items = getResources().getStringArray(R.array.rooms);
        builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                roomsText.setText(items[which]);
            }
        });
        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        roomsText.setText(items[0]);
    }

    @OnClick(R.id.furnishingText)
    public void furnishingTextClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Furnishing");
        final String[] items = getResources().getStringArray(R.array.furnishing);
        builder.setSingleChoiceItems(items, 0,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        furnishingText.setText(items[which]);
                    }
                });
        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        furnishingText.setText(items[0]);
    }

    @OnClick(R.id.noticeUnitText)
    public void noticeUnitTextClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Time");
        final String[] items = getResources().getStringArray(R.array.noticeUnit);
        builder.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                noticeUnitText.setText(items[which]);
            }
        });
        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        noticeUnitText.setText(items[0]);
    }

    @OnClick(R.id.locationCard)
    public void locationCardClick() {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("HardwareIds")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                locationText.setText(place.getAddress());
                forwardIcon.setClickable(true);
            }
        }
        if (requestCode == REQUEST_PHONE_STATE_PERMISSION) {
            if (resultCode == RESULT_OK) {
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (telephonyManager != null) {
                    if (android.support.v4.app.ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                    } else {
                        imei = telephonyManager.getDeviceId();
                    }
                }
            }
        }
    }
}
