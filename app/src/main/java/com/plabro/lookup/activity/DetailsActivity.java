package com.plabro.lookup.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v13.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.plabro.lookup.R;
import com.plabro.lookup.Util.Constants;
import com.plabro.lookup.Util.LinePagerIndicatorDecoration;
import com.plabro.lookup.Util.Utility;
import com.plabro.lookup.adapters.EditListImageAdapter;
import com.plabro.lookup.db_models.Record;
import com.plabro.lookup.models.Listing;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_CAMERA_PERMISSION = 1;
    static boolean isEdit = false;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.delete)
    ImageView delete;
    @BindView(R.id.imageRecycler)
    RecyclerView imageRecycler;
    @BindView(R.id.addImage)
    FloatingActionButton addImage;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.edit)
    CardView edit;
    @BindView(R.id.share)
    CardView share;
    @BindView(R.id.propertyForText)
    TextView propertyForText;
    @BindView(R.id.propertyTypeText)
    TextView propertyTypeText;
    @BindView(R.id.askingPriceText)
    TextView askingPriceText;
    @BindView(R.id.areaText)
    TextView areaText;
    @BindView(R.id.roomsText)
    TextView roomsText;
    @BindView(R.id.localText)
    TextView localText;
    @BindView(R.id.floorText)
    TextView floorText;
    @BindView(R.id.floorCountText)
    TextView floorCountText;
    @BindView(R.id.furnishingText)
    TextView furnishingText;
    @BindView(R.id.addInfoText)
    TextView addInfoText;
    @BindView(R.id.noticeText)
    TextView noticeText;
    @BindView(R.id.ownerNameText)
    TextView ownerNameText;
    @BindView(R.id.phoneText)
    TextView phoneText;
    @BindView(R.id.addressText)
    TextView addressText;
    @BindView(R.id.locationText)
    TextView locationText;
    @BindView(R.id.editandshare)
    LinearLayout editandshare;
//    @BindView(R.id.buy)
//    CardView buycard;
    String TAG = "AAA";
    String mCurrentPhotoPath;
    List<String> photoPathList = new ArrayList<>();
    List<String> photoBase64List = new ArrayList<>();
    EditListImageAdapter imageAdapter;
    Listing listing = new Listing();
    Record record;
    int recordId = 0;
    boolean newRecord = false;

    /**
     * Lifecycle methods
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_listing);
        ButterKnife.bind(this);

        //Check intents
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getBoolean(Constants.OPEN_CAMERA)) {
                openCamera();
                newRecord = true;
            }

            if (getIntent().getExtras().get(Constants.DETAIL_RECORD_DATA) != null) {
                listing = getIntent().getExtras().getParcelable(Constants.DETAIL_RECORD_DATA);
                loadExistingData(listing);
            } else {
                loadNewData();
            }
        }

        //Initialize image recyclerView, adapter, layout manager, snapHelper and indicator
        imageAdapter = new EditListImageAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        imageRecycler.setLayoutManager(layoutManager);
        imageRecycler.setHasFixedSize(true);
        imageRecycler.setAdapter(imageAdapter);
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(imageRecycler);
        imageRecycler.addItemDecoration(new LinePagerIndicatorDecoration());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (photoPathList.size() > 0) {
            imageAdapter.swapData(photoPathList);

            String im = "";
            for (int i = 0; i < photoBase64List.size(); i++) {
                if (i == 0) {
                    im = photoBase64List.get(i);
                } else {
                    im = im + "," + photoBase64List.get(i);
                }
            }
            listing.setImage_url(im);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (photoPathList.size() > 0) {
            try {
                if (newRecord) {
                    record.setListing(new Gson().toJson(listing, Listing.class));
                    record.save();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void loadNewData() {
        List<Record> recordSize = Record.listAll(Record.class);
        recordId = recordSize.size() + 1;
        title.setText("List " + recordId);
        record = new Record();
        record.setRecordId(recordId);
        date.setText(Utility.returnDateText(String.valueOf(System.currentTimeMillis())));
        listing.setId(recordId);
        listing.setInsertion_time(String.valueOf(System.currentTimeMillis()));
        listing.setListing_name("List " + recordId);
        listing.setSaleRent("EDIT");
        listing.setProp_type("EDIT");
        listing.setLocation("Get Location");
        locationText.setText(listing.getLocation());
        listing.setImage_url("");
        listing.setAskingPrice("0");
        listing.setArea("0");
        listing.setBhk("EDIT");
        listing.setLocality("EDIT");
        listing.setFloorNo("0");
        listing.setTotalFloors("0");
        listing.setFurnishing("EDIT");
        listing.setAdditionalDetails("EDIT");
        listing.setVisitNotice("0");
        listing.setOwnerName("EDIT");
        listing.setOwnerNumber("EDIT");
        listing.setOwnerAddress("EDIT");
    }

    public void loadExistingData(Listing listing) {
        try {
            title.setText(listing.getListing_name());
            propertyForText.setText(listing.getSaleRent());
            propertyTypeText.setText(listing.getProp_type());
            localText.setText(listing.getLocation());
            askingPriceText.setText(listing.getAskingPrice());
            areaText.setText(listing.getArea());
            locationText.setText(listing.getLocation());
            roomsText.setText(listing.getBhk());
            localText.setText(listing.getLocality());
            floorText.setText(listing.getFloorNo());
            floorCountText.setText(listing.getTotalFloors());
            furnishingText.setText(listing.getFurnishing());
            addInfoText.setText(listing.getAdditionalDetails());
            noticeText.setText(listing.getVisitNotice());
            ownerNameText.setText(listing.getOwnerName());
            phoneText.setText(listing.getOwnerNumber());
            addressText.setText(listing.getOwnerAddress());

            if (listing.getImage_url().contains(",")) {
                String[] sp = listing.getImage_url().split(",");
                photoBase64List.addAll(Arrays.asList(sp));
                photoPathList.addAll(Arrays.asList(sp));
            } else {
                photoBase64List.add(listing.getImage_url());
                photoPathList.add(listing.getImage_url());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Button click methods
     */

    @OnClick(R.id.back)
    public void onBackClicked() {
        finish();
    }

    @OnClick(R.id.delete)
    public void onDeleteClicked() {
        finish();
    }

    @OnClick(R.id.addImage)
    public void onAddImageClicked() {
        openCamera();
    }

    @OnClick(R.id.edit)
    public void onEditClicked() {
        try {
            if (newRecord) {
                record.setListing(new Gson().toJson(listing, Listing.class));
                record.save();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        isEdit = true;

        Intent intent = new Intent(this, EditDetailsActivity.class);
        intent.putExtra(Constants.DETAIL_RECORD_ID, recordId);
        intent.putExtra(Constants.DETAIL_RECORD_DATA, new Gson().toJson(listing, Listing.class));
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.locationCard)
    public void locationCardClick() {
        if (!locationText.getText().toString().equals("") && !locationText.getText().toString().contains("Get Location")) {
            String map = "http://maps.google.co.in/maps?q=" + locationText.getText().toString();
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
            startActivity(intent);
        }
    }

    /**
     * Camera related methods
     */

    void openCamera() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        } else {
            dispatchTakePictureIntent();
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.plabro.lookup.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File imageFile = null;
        try {
            imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        if (imageFile != null) {
            mCurrentPhotoPath = imageFile.getAbsolutePath();
        }
        return imageFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            photoPathList.add(mCurrentPhotoPath);

            photoBase64List.add(mCurrentPhotoPath);

            dispatchTakePictureIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamera();
                }
            }
        }
    }

//    private class Base64StringToFile extends AsyncTask<Void, Void, Void> {
//
//        String base64String;
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//            Bitmap bm = BitmapFactory.decodeFile(mCurrentPhotoPath);
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            bm.compress(Bitmap.CompressFormat.JPEG, 30, baos); //bm is the bitmap object
//            byte[] b = baos.toByteArray();
//            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
//            base64String = encodedImage;
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            super.onPostExecute(aVoid);
//            photoBase64List.add(base64String);
//        }
//    }
}
