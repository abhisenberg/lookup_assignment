package com.plabro.lookup.activity;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v13.app.FragmentPagerAdapter;
import android.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.plabro.lookup.R;
import com.plabro.lookup.Util.Constants;
import com.plabro.lookup.fragment.AllPostsFragment;
import com.plabro.lookup.fragment.MyPostFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowListsActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    public static int id;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.capture)
    ImageView capture;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tablayout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_lists);
        ButterKnife.bind(this);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager (ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(new AllPostsFragment(), "All Listings");
        adapter.addFragment(new MyPostFragment(), "My Listings");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment (Fragment fragment, String title){
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @OnClick(R.id.back)
    public void closeActivity() {
        finish();
    }

    @OnClick(R.id.capture)
    public void openDetail() {
        Intent intent = new Intent(this, DetailsActivity.class);
        //TODO: Open the details properly
        intent.putExtra(Constants.OPEN_CAMERA, true);
        startActivity(intent);
    }

}
