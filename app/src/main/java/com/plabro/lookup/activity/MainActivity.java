package com.plabro.lookup.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.plabro.lookup.R;
import com.plabro.lookup.fragment.ChoiceFragment;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    ChoiceFragment choiceFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        openChoiceFragment();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void openChoiceFragment() {
        choiceFragment = new ChoiceFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(android.R.id.content, choiceFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
